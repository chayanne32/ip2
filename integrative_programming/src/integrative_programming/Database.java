/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package integrative_programming;

import java.sql.*;

/**
 *
 * @author Administrator
 */
public class Database {
    public static Connection conn;
    public static Statement stmt;
    public static ResultSet rs;
    public static Database db;
    
    public String link = "jdbc:mysql://localhost:3306/ip2";
    public String user = "root";
    public String pass = "";
    
    public Database(){
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            this.conn = DriverManager.getConnection(link, user, pass);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    public static Database getDatabase(){
        
        if(db == null){
            db = new Database();
        }
        
        return db;
    }
    
}
